-- constant.e - Global constants - HIP 2.1
-- Copyright (C) 2001  Davi Tassinari de Figueiredo
--
-- This program is distributed under the terms of the GNU General
-- Public License. Please read the documentation for more information.
--
-- This file has constants used in several parts of HIP.


-- Colors in a RGB tuple
global constant  PAL_RED = 3, PAL_GREEN = 2, PAL_BLUE = 1

-- img_info structure
global
constant IMG_WIDTH = 1, IMG_HEIGHT = 2, IMG_BPP = 3,
	 IMG_HRES = 4, IMG_VRES = 5,
	 IMG_PAL = 6, IMG_COMPRESSION = 7,
	 IMG_BACKGROUNDCOLOR = 8,
	 IMG_INTERLACED = 9,        -- IMG_FORMAT = 9,
	 IMG_TRANSPARENT = 10,
	 IMG_ADDRESS = 11, IMG_FORMAT = 12,

	 IMG_INFO_SIZE = 12,        -- size of img_info


	 -- information specific to HIP
	 HIP_TRANSPARENT_PIXELS = 1, HIP_AVAILABLEBYTES = 2,
	 HIP_SORTEDPAL = 3, HIP_MAXBITSPERBYTE = 4,

	 HIP_INFO_SIZE = 4,



	 IMG_INFO = 1, HIP_INFO = 2,
	 PIC_INFO_SIZE = 2

	 -- graphic format information


-- Error codes
global constant ERROR_FILENOTFOUND = -1, ERROR_CORRUPTED = -2,
		ERROR_UNSUPPORTED = -3, ERROR_FILEERROR = -4,
		ERROR_FILETOOBIG = -5, ERROR_NOHIDDENFILE = -6,
		ERROR_OUTOFMEMORY = -7, ERROR_INVALIDBMP = -8,
		ERROR_UNSUPPORTEDBPP = -9

