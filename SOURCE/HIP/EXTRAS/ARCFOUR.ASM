; Arcfour encryption routine in Assembly
; Copyright (C) 2002 Davi T. Figueiredo
;
; Used in arcfour.e (version 1.12), available from http://www16.brinkster.com/davitf/
;
; Export or use of this program may be restricted in some countries.
; Please read arcfour.txt for more information and for its license terms
; and disclaimer.

push eax
push ebx
push ecx
push edx
push edi
push esi

xor eax,eax         ; EAX = 0
xor edx,edx         ; EDX = 0

; Read A and B
mov al, [a_address] ; AL = A
mov ah, [b_address] ; AH = B

mov ecx, state_address   ; ECX = &state[0]
mov esi, data_address    ; ESI = &data

Char:

inc al              ; A = A + 1

xor edx, edx        ; EDX = 0
mov dl, al          ; DL = A  (EDX = A)
mov edi, edx        ; EDI = A
add edi, ecx        ; EDI contains address of state[A]

mov bl, [edi]       ; BL = state[A]
add ah, bl          ; B = B + state[A]

mov dl, ah          ; DL = B  (EDX = B)
add edx, ecx        ; EDX contains address of state[B]
mov bh, [edx]       ; BH = state[B]

; swap state[A] and state[B]
mov [edx], bl       ; state[B] = state[A]
mov [edi], bh       ; state[A] = state[B] (the old one)

xor edx, edx        ; EDX = 0
mov dl, bl          ; DL = state[A]
add dl, bh          ; DL = state[A] + state[B]

mov dl, [ecx+edx]   ; DL = state [ state[A] + state[B] ]

xor [esi], dl       ; xor data with DL
inc esi             ; move to next char in data

cmp esi,max_data_address    ; All the characters have been processed?
jb Char                     ; If not, go back to char

mov [a_address],al          ; Store A
mov [b_address],ah          ; Store B

pop esi
pop edi
pop edx
pop ecx
pop ebx
pop eax
ret

