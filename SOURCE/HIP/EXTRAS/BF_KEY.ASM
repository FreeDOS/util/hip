; Blowfish key encryption routine in Assembly
; Copyright (C) 2002 Davi T. Figueiredo
;
; Used in bf.e (version 2.02), available from http://www16.brinkster.com/davitf/
;
; Export or use of this program may be restricted in some countries.
; Please read bf.txt for more information and for its license terms
; and disclaimer.

; The difference between this routine and the actual encryption routine
; is that this one does not invert the byte order of the plaintext and
; ciphertext. See bf_enc.asm for some more information about the code.


PUSH EAX                    ;save initial values
PUSH EBX
PUSH ECX
PUSH EDX


MOV ECX,p_address

;EAX will store xL and EBX will store xR

MOV EAX,[x_address]
MOV EBX,[x_address_4]


; This is very similar to bf_enc.asm, so please see the comments there.

Round:

XOR EAX,[ECX]


PUSH EAX
PUSH EBX


MOV EDX,EAX       ; Calculate F(xL)
SHR EDX,22
AND EDX,#000003FC
ADD EDX,s1_address
MOV EBX,[EDX]

MOV EDX,EAX
SHR EDX,14
AND EDX,#000003FC
ADD EDX,s2_address
ADD EBX,[EDX]

MOV EDX,EAX
SHR EDX,6
AND EDX,#000003FC
ADD EDX,s3_address
XOR EBX,[EDX]

MOV EDX,EAX
SHL EDX,2
AND EDX,#000003FC
ADD EDX,s4_address
ADD EBX,[EDX]   ;EBX now contains the result of F


POP EAX         ; Update xL and xR and swap them
XOR EAX,EBX
POP EBX


ADD ECX,4   ; Move to next P
CMP ECX,p_address_64    ; Finished all 16 rounds?
JB  Round           ; No, so go back for next round


XOR EAX,[ECX]   ;Do the last xorings
ADD ECX,4
XOR EBX,[ECX]

MOV [x_address],EBX     ;unswap and save xL and xR
MOV [x_address_4],EAX


POP  EDX                    ;restore initial values
POP  ECX
POP  EBX
POP  EAX

RET                         ;end

