; remaind.asm - ASM source for remaind, in rand.e - HIP 2.0
;
; This program is distributed under the terms of the GNU General
; Public License. Please read the documentation for more information.


push eax
push ebx
push ecx
push edx
push edi
push esi


mov ecx, data_address
mov edi, total
mov ebx, divisor

Remainder:

mov eax, [ecx]              ; Get data
xor edx, edx                ; Zero EDX
div ebx                     ; Divide EAX by EBX
mov [ecx], edx              ; Write remainder

add ecx, 4                  ; Move to next number
dec edi                     ; Decrease counter
cmp edi, 0                  ; Any numbers yet?
jne Remainder


pop esi
pop edi
pop edx
pop ecx
pop ebx
pop eax
ret

