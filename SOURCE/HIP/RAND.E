-- rand.e - Pseudo-random number generator - HIP 2.0
-- Copyright (C) 2001  Davi Tassinari de Figueiredo
--
-- This program is distributed under the terms of the GNU General
-- Public License. Please read the documentation for more information.
--
-- This is a pseudo-random number generator based in the Arcfour
-- encryption algorithm. It generates a different set of numbers for
-- each key. The method used here is intended to be cryptographically
-- secure.
--
-- The ASM sources for remaind, arcf_init and arcfour_enc are in the
-- files remaind.asm, arcfinit.asm and arcfour.asm.


include machine.e

constant numbers_per_call = 1024,   -- Numbers computed in each call to gen_random
	 bytes_per_call = numbers_per_call * 4,
	 data_address = allocate (bytes_per_call)

-- The following lines of code have been adapted from arcfour.e
constant state_address=allocate(256),
	 a_address=allocate(2),b_address=a_address+1,
	 key_address = allocate(256)

-- State initialization routine
constant arcf_init = allocate(88)
poke( arcf_init, and_bits(#FF,
"�����˦5�G.uuuu�@�x�s5�u�l�P3uuuu�t��}w��uH��������}��tuuuu�w�t��"-117&
"����n؞���������Z[VUWT�"+4))
poke4(arcf_init + 11, state_address)
poke4(arcf_init + 29, key_address)
poke4(arcf_init + 72, a_address)
poke4(arcf_init + 77, b_address)
constant key_length_address = arcf_init + 55

-- Data generation routine
constant arcfour_enc = allocate(93)
poke(arcfour_enc, and_bits(#FF,
"�������h�zH����2ͨ���a����f�����h�z0j1�w2Ǩ�0��r2�0�0��z0���2��ؾ"+88&
"E������qӡ�����$����]^YXZW�"+1))
poke4(arcfour_enc + 11, a_address)
poke4(arcfour_enc + 17, b_address)
poke4(arcfour_enc + 22, state_address)
poke4(arcfour_enc + 76, a_address)
poke4(arcfour_enc + 82, b_address)
poke4(arcfour_enc + 27, data_address)
poke4(arcfour_enc + 69, data_address + bytes_per_call)


-- This routine computes the remainder of a sequence of 32-bit numbers
-- by a certain divisor, and writes the result over the original number
constant remaind = allocate(45)
poke(remaind, and_bits(#FF,"ILJKPO�����������������*���\n|��H|��n�WXSRTQ�"+7))
poke4(remaind + 7, data_address)
poke4(remaind + 12, numbers_per_call)

global procedure init_remainder (atom divisor)
    -- Store the divisor in the appropriate place
    poke4(remaind + 17, divisor)
end procedure



atom numbers_used
sequence numbers

procedure gen_random()
    -- Clean memory for writing the data
    mem_set (data_address, 0, bytes_per_call)

    -- Call asm routine to generate the pseudo-random bytes
    call(arcfour_enc)

    -- Call asm routine to compute the remainder of the 32-bit numbers
    call (remaind)

    -- Get numbers and add 1 (because in Euphoria indexes start at 1)
    numbers = peek4u ( {data_address, numbers_per_call}) + 1

    -- No numbers have been used yet
    numbers_used = 0
end procedure


global function random ()

    -- Have all of the computed numbers been used?
    -- If so, generate more numbers
    if numbers_used >= numbers_per_call then gen_random() end if

    -- One more number has been used
    numbers_used += 1

    return numbers [numbers_used]   -- Return number

end function


global procedure init_random (sequence key, atom max)
    -- Generate PRNG state based on key
    atom junk

    -- Write key length
    poke4(key_length_address, length(key))

    -- Write key
    poke(key_address, key)

    -- Call initialization routine
    call(arcf_init)

    -- Initialize remainder routine
    init_remainder (max)

    -- Force number generation on the first call to random()
    numbers_used = numbers_per_call

    -- Discard the first 256 numbers generated, further initializing the generator
    for n = 1 to 256 do
	junk = random()
    end for

end procedure

global function rand_bytes ()
    -- returns a sequence of bytes_per_call random bytes

    -- Clean memory for writing the data
    mem_set (data_address, 0, bytes_per_call)

    -- Call asm routine to generate the pseudo-random bytes
    call(arcfour_enc)

    -- Get the pseudo-random bytes from memory
    return peek ({data_address, bytes_per_call})


end function

